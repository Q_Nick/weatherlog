<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Data;
use DB;
use Carbon\Carbon;

class DataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $actual=DB::table('data')->get()->last();
        $last24hData=DB::table('data')->orderBy('created_at', 'desc')->limit(106)->get();

        $last24hDataFiltered=[];

        $recordCounter=-1;

        foreach($last24hData as $item){

            $recordCounter++;

            if($recordCounter % 4 !=0) continue;

            $record=[
                'temperature'=>$item->temperature,
                'pressure'=>$item->pressure,
                'humidity'=>$item->humidity,
                'time'=>date('H', strtotime($item->created_at))];

                array_push($last24hDataFiltered, $record);
        }

        return view('getData', ['actual'=>$actual, 'last24hData'=>array_reverse($last24hDataFiltered)]);
    }

    public function new($token, $temperature, $pressure, $humidity){
        $sensor=DB::table('sensors')->where('token', $token)->get();

        DB::table('data')->insert([
            'sensor'=>$sensor->first()->id,
            'temperature'=>$temperature,
            'pressure'=>$pressure,
            'humidity'=>$humidity,
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now()
        ]);

        return response()->json(['created'=>true], 201);
    }

    public function store(Request $request){
        echo '<pre>';

        foreach ($request->data as $record){

            DB::table('data')->insert([
                'sensor'=>DB::table('sensors')->where('token', $record['token'])->get()->first()->id,
                'temperature'=>$record['temperature'],
                'pressure'=>$record['pressure'],
                'humidity'=>$record['humidity'],
                'date'=>date("Y-m-d H:i:s", $record['time']),
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ]);
        }
    }
}
