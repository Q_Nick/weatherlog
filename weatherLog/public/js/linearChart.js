function linearChart(elementId, label, dataX, dataY, backgroundColor, borderColor) {
    var ctx = document.getElementById(elementId).getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: dataX,
            datasets: [{
                label: label,
                data: dataY,
                backgroundColor: [
                    backgroundColor
                ],
                borderColor: [
                    borderColor
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
}

function linearCharts(elementId, dataX, datasets) {
    var ctx = document.getElementById(elementId).getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: dataX,
            datasets: datasets
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false
                    }
                }]
            }
        }
    });
}

class dataset {
    label;
    data;
    backgroundColor;
    borderColor;
    borderWidth = 1;

    constructor(_label, _data, _backgroundColor, _borderColor) {
        this.label = _label;
        this.data = _data;
        this.backgroundColor = [_backgroundColor];
        this.borderColor = [_borderColor];
    }
}