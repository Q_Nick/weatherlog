@extends('layout')
@section('place', 'Grabownica Starzeńska')

@section('content')
<h1>Wyniki pomiarów</h1>

<div class="row">
    <div class="col-lg-4 col-sm-12 actual-data">
        <div class="actual-data-panel">
            <label>Temperatura</label>
            <span class="content">
                <span class="value">{{$actual->temperature}}</span>
                <span class="unit">°C</span>
            </span>
        </div>
    </div>
    <div class="col-lg-4 col-sm-12 actual-data">
        <div class="actual-data-panel">
            <label>Ciśnienie atmosferyczne</label>
            <span class="content">
                <span class="value">{{$actual->pressure}}</span>
                <span class="unit">hPa</span>
            </span>
        </div>
    </div>
    <div class="col-lg-4 col-sm-12 actual-data">
        <div class="actual-data-panel">
            <label>Wilgotność powietrza</label>
            <span class="content">
                <span class="value">{{$actual->humidity}}</span>
                <span class="unit">%</span>
            </span>
        </div>
    </div>
</div>

<canvas id="temperature"></canvas>
<canvas id="pressure"></canvas>
<canvas id="humidity"></canvas>

<script>
        const data=@json($last24hData);
        const time=[];
        for(index in data){
            time.push(data[index].time);
        }
        const temperature=[];
        for(index in data){
            temperature.push(data[index].temperature);
        }
        const humidity=[];
        for(index in data){
            humidity.push(data[index].humidity);
        }
        const pressure=[];
        for(index in data){
            pressure.push(data[index].pressure);
        }

        const temperatureLine=new dataset("temperatura", temperature, 'rgba(202, 51, 109, 0.5)', 'rgb(202, 51, 109)')
        const pressureLine=new dataset("ciśnienie", pressure, 'rgba(94, 201, 94, 0.5)', 'rgb(94, 201, 94)')
        const humidityLine=new dataset("wilgotność", humidity, 'rgba(110, 199, 199, 0.5)', 'rgb(110, 199, 199)')

        linearCharts("temperature", time, [temperatureLine])
        linearCharts("pressure", time, [pressureLine])
        linearCharts("humidity", time, [humidityLine])
      
      </script>


@endsection